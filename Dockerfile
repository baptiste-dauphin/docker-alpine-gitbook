FROM alpine:3.10
RUN apk add "npm=10.19.0-r0"
RUN npm install gitbook-cli -g # install gitbook
RUN gitbook fetch 3.2.3 # fetch final stable version
RUN gitbook install
CMD ["/bin/sh"]
